<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
		<meta name="generator" content="Jekyll v3.8.5">
		<title>Renovación de Autoabasto</title>

		<!-- Bootstrap core CSS -->
		<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">

		<style>
		.bd-placeholder-img {
			font-size: 1.125rem;
			text-anchor: middle;
			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
		}

		@media (min-width: 768px) {
			.bd-placeholder-img-lg {
			font-size: 3.5rem;
			}
		}
		</style>
		<!-- Custom styles for this template -->
		<link href="css/dashboard.css" rel="stylesheet"/>
		<link rel="stylesheet" href="css/estilo.css"/>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.8.0/sweetalert2.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
		<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet" >
		<script src="https://code.jquery.com/jquery-3.4.1.js"></script>	
		<script src="js/bootstrap.min.js"></script>
		<script src="js/bootstrap.bundle.min.js"></script>
		<script src="js/feather.min.js"></script>
		<script src="js/Chart.min.js"></script>
		<script src="js/ajax.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.8.0/sweetalert2.min.js"></script>
		<script type="module" src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons/ionicons.esm.js"></script>
		<script nomodule="" src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons/ionicons.js"></script> 
		<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
	</head>
	<script language='javascript'>
		function sumar()
		{
			var valor1 = document.getElementById('edreqdomestico').value;
			var valor2 = document.getElementById('edreqcomercial').value;
			var valor3 = document.getElementById('edreqindustrial').value;
			var valor4 = document.getElementById('edreqmixto').value;
			
			var suma = parseInt(valor1) + parseInt(valor2) + parseInt(valor3) + parseInt(valor4)
			/*Display.innerHTML = suma;*/
			document.getElementById('edtotalunidades').value = suma
			//alert(suma);
		}
	</script>
	
<?php 

include_once 'conexion.php';

$caso=$_POST['selectIdCase'];
$folio=$_POST['selectRadNumber'];
  
  $consulta= "select top 1 ex.Noexpediente, sp.Folio, sp.Fecha,sp.Correo,r.seleccionado,af.No_Factibilidad,c.Cambioprorroga,r.Nombre,r.Telefono,r.Correo,
  ex.reqdomestico,ex.reqcomercial, ex.reqindustrial, ex.reqmixto,ex.Totalunidades
  from Expediente ex 
  left join SolicitudPadron sp on ex.SolicitudPadron=sp.idSolicitudPadron 
  left join A_Factibilidades af on af.No_Expediente=ex.Noexpediente 
  left join Cedula c on c.idCedula=ex.Cedula
  left join Representantes r on r.Expediente=ex.idExpediente
  where sp.Folio ='$folio'
  group by ex.Noexpediente,sp.Folio, sp.Fecha,sp.Correo,af.No_Factibilidad,c.Cambioprorroga,r.Nombre,r.Telefono,r.Correo,
  ex.reqdomestico,ex.reqcomercial, ex.reqindustrial, ex.reqmixto,ex.Totalunidades,r.seleccionado
  order by No_Factibilidad desc"; 
  //echo $consulta;
  $sql=sqlsrv_query($conn,$consulta);
  $values = sqlsrv_fetch_array($sql);

  $fechae=$values['Fecha'];
  $correoe=$values['Correo'];
  $noExpediente=$values['Noexpediente'];
  $numFactibili=$values['No_Factibilidad'];
  $motivoProrroga=$values['Cambioprorroga'];
  $nombreRe=$values['Nombre'];
  $telefenoRe=$values['Telefono'];
  $correoRe=$values[8];
  $SeleccionadoREP=$values['seleccionado'];
  $reqdomestico=$values['reqdomestico'];
  $reqcomercial=$values['reqcomercial'];
  $reqindustrial=$values['reqindustrial'];
  $reqmixto=$values['reqmixto'];
  $Totalunidades=$values['Totalunidades'];

  $sqlObs=" select ob.Observacion from Expediente ex 
  inner join SolicitudPadron sp on sp.idSolicitudPadron = ex.SolicitudPadron
  inner join Cedula c on c.idCedula = ex.Cedula 
  inner join Observaciones ob on ob.Expediente = ex.idExpediente
  where ex.Noexpediente = '$noExpediente' and sp.Folio = '$folio'";
  
  $sql1=sqlsrv_query($conn,$sqlObs);
  $result = sqlsrv_fetch_array($sql1);
  $date=$values['Fecha']; 
  $fechafin=date_format($date,'Y-m-d'); 
  $obseva=$result['Observacion'];
 // $observa2=utf8_encode($obseva);
	$form ="<h4>1.- Coloca el correo </h4>";
	$form.="<form id='edita-caso' name='miformulario' method='POST' enctype='multipart/form-data'>";
	$form.="<div class='form-group row'>";
	$form.="<div class='form-group col-md-4'>";
	$form.="<label>Folio:</label>";
	$form.="<input type='text' id='numCaso' name='numCaso' class='form-control' readonly='readonly' value='$caso'/>";
	$form.="<input type='text' id='numRadNumber' name='numRadNumber' class='form-control' readonly='readonly' value='$folio'/>"; 
	$form.="</div>";

	$form.="<div class='form-group col-md-6'>";
	$form.="<label>Opción de Trámite:</label>";
	$form.="<input type='text' id='proceso' name='proceso' value='Ampliación de Factibilidad' class='form-control' readonly='readonly'>";
	$form.="</div>";
	$form.="<div class='form-group col-md-4'>";
	$form.="<label>Fecha:</label>";
	$form.="<input type='text' id='editFecha' name='editFecha' class='form-control' value='$fechafin' readonly='readonly'>";
	$form.="</div>";
	$form.="<div class='form-group col-md-6'>";
	$form.="<label>Observaciones</label>";

	$form.="<textarea name='observa' class='form-control' readonly='readonly'>$obseva</textarea>";
	$form.="</div>";
	$form.="<div class='form-group col-md-10'>";
	$form.="<label>Correo electronico para notificar:</label>";
	$form.="<input type='email' id='inEmail' name='correo' class='form-control' required='required' value='$correoe' onkeyup='add();'readonly='readonly'>";
	$form.="</br>";
	
	$form.="<h4>2.- Introduce el numero de expediente</h4>";
	$form.="</div>";
	$form.="<div class='form-group col-md-5'>";
	$form.="<label>No_Expediente</label>";
	$form.="<div class='input-group mb-5'>";
	$form.="<input type='text' id='n_expediente' name='n_expediente' class='form-control' value='$noExpediente' aria-label='# de Expediente' aria-describedby='button-addon2' readonly='readonly'>";
	$form.="<div class='input-group-append'>";
	$form.="</div>";
	$form.="</div>";
	$form.="</div>";
	$form.="<div class='form-group col-md-4'>";
	$form.="<label>Seleccionar factibilidad:</label>";
	$form.="<div id='select2lista'><input type='text' id='num_fac' name='num_fac' value='$numFactibili' class='form-control' readonly='readonly'></div>";
	$form.="<div id='select3lista'></div>";
	$form.="</div>";
	$form.="<div class='form-group col-md-2'>";
	$form.="</div>";

	$form.="<div class='form-group col-md-10'>";
	$form.="<h2>Representante Legal</h2>";
	$form.="<h4>3.- Escriba los siguientes datos del representante legal</h4>";
	$form.="<table class='table table-hover'>";
	$form.="<thead>";
	$form.="<tr>";
	$form.="<th scope='col'><label>Nombre:</label></th>";
	$form.="<th scope='col'><label>Telefono:</label></th>";
	$form.="<th scope='col'><label>Correo:</label></th>";
	$form.="<th scope='col'><label>Seleccionar:</label></th>";
	$form.="</tr>";
	$form.="</thead>";
	$form.="<tbody>";
	$form.="<tr>";
	$form.="<th scope='row'><input type='text' id='nombreeditar' name='nombreeditar' class='form-control' value='$nombreRe' /></th>";
	$form.="<td><input type='text' id='telefonoeditar' name='telefonoeditar' class='form-control' value='$telefenoRe' /></td>";
	$form.="<td><input type='email' id='correoREPeditar' name='correoREPeditar' class='form-control entrada-usuario' value='$correoRe' /></td>";
	$form.="<td><input type='checkbox' id='SeleccionadoREPeditar' name='SeleccionadoREPeditar' class='form-control entrada-usuario' value='$SeleccionadoREP' /></td>";

	$form.="</tr>";
	$form.="</tbody>";
	$form.="</table>";
	$form.="</div>";
	$form.="</div>"; 

	$form.="<div class='form-group col-md-10'>";
	$form.="<h4>4.- Desglose de requerimiento</h4>";
	$form.="<table class='table table-hover'>";
	$form.="<tr>";
	$form.="<td><label> Doméstico</label></td>";
	$form.="<td><input type='text' id='edreqdomestico' name='edreqdomestico' class='form-control' onBlur='sumar()' value='$reqdomestico' /></td>";
	$form.="</tr>";
	$form.="<tr>";
	$form.="<td><label> Comercial</label></td>";
	$form.="<td><input type='text' id='edreqcomercial' name='edreqcomercial' class='form-control' onBlur='sumar()' value='$reqcomercial' /></td>";
	$form.="</tr>";
	$form.="<tr>";
	$form.="<td><label> Industrial</label></td>";
	$form.="<td><input type='text' id='edreqindustrial' name='edreqindustrial' class='form-control' onBlur='sumar()' value='$reqindustrial' /></td>";
	$form.="</tr>";
	$form.="<tr>";
	$form.="<td><input type='text' id='edreqespecificar' name='edreqespecificar' class='form-control' required placeholder='Otro' /></td>";
	$form.="<td><input type='text' id='edreqmixto' name='edreqmixto' class='form-control' onBlur='sumar()' value='$reqmixto' /></td>";
	$form.="</tr>";
	$form.="<tr>";
	$form.="<td><label> Total</label></td>";
	$form.="<td><input type='text' id='edtotalunidades' name='edtotalunidades' class='form-control' value='$Totalunidades' readonly='readonly'/></td>";
	$form.="</tr>";
	$form.="</table>";
	$form.="</div>"; 

    
printf($form);


?>
