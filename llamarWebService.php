<?php 
error_reporting(E_ERROR);
include 'conexion.php';

	function multiexplode ($delimiters,$string) {
		$ready = str_replace($delimiters, $delimiters[0], $string);
		$launch = explode($delimiters[0], $ready);
		return  $launch;
	}

	$radNumber11=$_POST['radNumber'];
	$radNumber = trim($radNumber11);
	$rfc=$_POST['rfc'];
	$factibilidades_slc=$_POST['factibilidades_slc'];
	$correo=$_POST['correo'];
	$Noexpediente1=$_POST['n_expediente'];
	$Noexpediente=trim($Noexpediente1);
	$ExisteexpedienteOR=1;
	
	$NoFactibilidad=$_POST['numFacti'];
	
	$query_histexp="SELECT Top 1 EP.idA_Expediente_prod1 AS Historicoexpediente,E.noregistro
	FROM SCG.dbo.A_Expediente_prod1 E
	INNER JOIN SCG.dbo.A_Expediente_prod1_EK EP ON E.noregistro=EP.noregistro 
	where E.noregistro='".$Noexpediente."' ";
	$queryExp = sqlsrv_query($conn, $query_histexp);
	$exphist=sqlsrv_fetch_array($queryExp);
	$Historicoexpediente=$exphist['Historicoexpediente'];

	$query_Facti="select top 1 id_Factibilidad,No_Expediente,No_Factibilidad,Expediente,Fecha_Vigencia,Unidades,Gasto,Id_Status
	from scg.dbo.Factibles 
	where No_Factibilidad='".$factibilidades_slc."'";
	$queryFacti = sqlsrv_query($conn, $query_Facti);
	$factiselect=sqlsrv_fetch_array($queryFacti);
	$idfactibilidad=$factiselect['id_Factibilidad'];
	$Fecha_Vigencia=$factiselect['Fecha_Vigencia'];
	$Unidades=$factiselect['Unidades'];
	$Gasto=$factiselect['Gasto'];
	$Id_Status=$factiselect['Id_Status'];
	//$Fecha_Vigencia2=date_format($Fecha_Vigencia,'Y-m-d H:i:s'); 
	
	$archivo=$_FILES['archivo']['name'];
	//print_r($archivo);
	$code1=$_POST['code1'];
	$archivo2=$_FILES['archivo2']['name'];
	$code2=$_POST['code2'];
	$archivo3=$_FILES['archivo3']['name'];
	$code3=$_POST['code3'];
	$archivo4=$_FILES['archivo4']['name'];
	$code4=$_POST['code4'];

	$nombre=$_POST['nombre'];
	$telefono=$_POST['telefono'];
	$correoREP=$_POST['correoREP'];
	$SeleccionadoREP=1;
	$nume_facti=$_POST['factibilidades_slc'];

	$reqdomestico=$_POST['reqdomestico'];
	$reqcomercial=$_POST['reqcomercial'];
	$reqindustrial=$_POST['reqindustrial'];
	$reqespecificar=$_POST['reqespecificar'];
	$reqmixto=$_POST['reqmixto'];
	$totalunidades=$_POST['totalunidades'];
	
	$query_rfc="SELECT iduser,userName FROM SCG.dbo.WFUSER
	WHERE userName='".$rfc."'";
	$queryRFC = sqlsrv_query($conn, $query_rfc);
	$rfcuser=sqlsrv_fetch_array($queryRFC);
	$iduserRFC=$rfcuser['iduser'];
	$hoy=date("d")."/".date("m")."/".date("Y");
	//echo $hoy;
	//echo "<BR>EXISTE EXPEDIENTE: ".$ExisteexpedienteOR;

//===========================================Realizar Actividad======================================================================  

	$soap_request1 = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" encoding="UTF-8">
	<soapenv:Header/>
	<soapenv:Body>
	<tem:performActivityAsString>
	<!--Optional:-->
	<tem:activityInfo> 
	<![CDATA[<BizAgiWSParam>
		<domain>cea</domain>
		<userName>'.$rfc.'</userName>
		<ActivityData>
			<radNumber>'.$radNumber.'</radNumber>
			<taskId>7219</taskId>
		</ActivityData>
		<Entities>
			<Expediente>
				<PadronDesarrolladores>
					<RFC>'.$rfc.'</RFC>
				</PadronDesarrolladores>
				<SolicitudPadron>
					<Folio>'.$radNumber.'</Folio>
					<Usuario>'.$iduserRFC.'</Usuario>
					<Fecha>'.$hoy.'</Fecha>
					<Tipomovimiento>9</Tipomovimiento>
					<Correo>'.$correo.'</Correo>
					<Existeexpediente>'.$ExisteexpedienteOR.'</Existeexpediente>
					<TipodeMovimientoFactibi>210</TipodeMovimientoFactibi>
				</SolicitudPadron>
			<Noexpediente>'.$Noexpediente.'</Noexpediente>			
			<Factibles>
				<No_Factibilidad>'.$idfactibilidad.'</No_Factibilidad>
			</Factibles>
			<Historicoexpediente>
				<noregistro>'.$Noexpediente.'</noregistro>
			</Historicoexpediente>
			<Documentoss>    
				<Documentos>
					<Tipo>168</Tipo>
					<Entregado>1</Entregado>
					<Documento><File fileName="'.$archivo.'">'.$code1.'</File></Documento>
				</Documentos>
				<Documentos>
					<Tipo>1067</Tipo>
					<Entregado>1</Entregado>
                    <Documento><File fileName="'.$archivo2.'">'.$code2.'</File></Documento>
				</Documentos>
				<Documentos>
					<Tipo>221</Tipo>
					<Entregado>1</Entregado>
                    <Documento><File fileName="'.$archivo3.'">'.$code3.'</File></Documento>
				</Documentos>
				<Documentos>
					<Tipo>267</Tipo>
					<Entregado>1</Entregado>
                    <Documento><File fileName="'.$archivo4.'">'.$code4.'</File></Documento>
				</Documentos>
			</Documentoss>    
			<Representantes>
			<Representantes>
				<Nombre>'.$nombre.'</Nombre>
				<Telefono>'.$telefono.'</Telefono>
				<Correo>'.$correoREP.'</Correo>  
				<Seleccionado>'.$SeleccionadoREP.'</Seleccionado>                 
			</Representantes>                        
			</Representantes> 
			<Cedula>
				<Reqdomestico>'.$reqdomestico.'</Reqdomestico>
				<Reqcomercial>'.$reqcomercial.'</Reqcomercial>
				<Reqindustrial>'.$reqindustrial.'</Reqindustrial>
				<Reqespecificar>'.$reqespecificar.'</Reqespecificar>
				<Reqmixto>'.$reqmixto.'</Reqmixto>
				<Viviendas>'.$totalunidades.'</Viviendas>
			</Cedula>
			<No_Factibilidad>
				<No_Factibilidad>'.$factibilidades_slc.'</No_Factibilidad>
				<No_Expediente>'.$Noexpediente.'</No_Expediente>
				<Propietario>'.$rfc.'</Propietario>
				<Unidades>'.$Unidades.'</Unidades>
				<Gasto_requerido>'.$Gasto.'</Gasto_requerido>
				<Seleccion>1</Seleccion>
			</No_Factibilidad>
			</Expediente>
		</Entities>
	</BizAgiWSParam>]]>
	</tem:activityInfo>
	</tem:performActivityAsString>
	</soapenv:Body>
	</soapenv:Envelope>';

    $headers1 = array(
	"Content-type: text/xml; charset=utf-8",
	"Accept: text/xml",
	"Cache-Control: no-cache",
	"Pragma: no-cache",
	"SOAPAction: http://tempuri.org/performActivityAsString",
	"Content-length: ".strlen($soap_request1),
    );

    $url1 = "http://10.1.1.67/SCG/WebServices/WorkflowEngineSOA.asmx";
    //$url1 = "http://10.1.1.155/SCG/WebServices/WorkflowEngineSOA.asmx";
    $ch1 = curl_init($url1);
	curl_setopt($ch1, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch1, CURLOPT_POST, 1);
	curl_setopt($ch1, CURLOPT_HTTPHEADER, $headers1);
	curl_setopt($ch1, CURLOPT_POSTFIELDS, $soap_request1);
	curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch1, CURLOPT_VERBOSE, true);
	curl_setopt($ch1, CURLOPT_TIMEOUT,10);

	$resultado = utf8_decode(curl_exec($ch1));
	curl_close($ch1);
	$datos = multiexplode(array("&lt;","/","&gt;"), $resultado);
	//var_dump($datos);
	$error=$datos['24'];
    echo $error;
	

    //============================================Enviar a CEA===========================================
	$fecha=date("Y-m-d H:i:s");

	$requiero='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" encoding="UTF-8">
	<soapenv:Header/>
	<soapenv:Body>
	<tem:performActivityAsString>
		<!--Optional:-->
		<tem:activityInfo> 
		<![CDATA[<BizAgiWSParam>
		<domain>cea</domain>
		<userName>'.$rfc.'</userName>
		<ActivityData>
			<radNumber>'.$radNumber.'</radNumber>
			<taskId>7226</taskId>
		</ActivityData>
		<Entities>
			<Expediente>
			<Historicoexpediente>'.$Historicoexpediente.'</Historicoexpediente>
			<Noexpediente>'.$Noexpediente.'</Noexpediente>
				<PadronDesarrolladores>
					<RFC>'.$rfc.'</RFC>
				</PadronDesarrolladores>
				<SolicitudPadron>
					<Folio>'.$radNumber.'</Folio>
					<Usuario>'.$iduserRFC.'</Usuario>
					<Fecha>'.$hoy.'</Fecha>
					<Tipomovimiento>9</Tipomovimiento>
					<Correo>'.$correo.'</Correo>
					<Existeexpediente>'.$ExisteexpedienteOR.'</Existeexpediente>
					<TipodeMovimientoFactibi>210</TipodeMovimientoFactibi>
				</SolicitudPadron>
			
			<No_Factibilidad>'.$NoFactibilidad.'</No_Factibilidad>                         
			<Cedula>
				<Reqdomestico>'.$reqdomestico.'</Reqdomestico>
				<Reqcomercial>'.$reqcomercial.'</Reqcomercial>
				<Reqindustrial>'.$reqindustrial.'</Reqindustrial>
				<Reqespecificar>'.$reqespecificar.'</Reqespecificar>
				<Reqmixto>'.$reqmixto.'</Reqmixto>
				<Viviendas>'.$totalunidades.'</Viviendas>
			</Cedula>
			</Expediente>
		</Entities>
	</BizAgiWSParam>]]>
	</tem:activityInfo>
	</tem:performActivityAsString>
	</soapenv:Body>
	</soapenv:Envelope>';

	$headers2 = array(
	"Content-type: text/xml; charset=utf-8",
	"Accept: text/xml",
	"Cache-Control: no-cache",
	"Pragma: no-cache",
	"SOAPAction: http://tempuri.org/performActivityAsString",
	"Content-length: ".strlen($requiero),
	);

    //$url2 = "http://10.1.1.155/SCG/WebServices/WorkflowEngineSOA.asmx";
    $url2 = "http://10.1.1.67/SCG/WebServices/WorkflowEngineSOA.asmx";
    $ch2 = curl_init($url2);

	curl_setopt($ch2, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch2, CURLOPT_POST, 1);
	curl_setopt($ch2, CURLOPT_HTTPHEADER, $headers2);
	curl_setopt($ch2, CURLOPT_POSTFIELDS, $requiero);
	curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch2, CURLOPT_VERBOSE, true);
	curl_setopt($ch2, CURLOPT_TIMEOUT,10);

    $resultado2 = utf8_decode(curl_exec($ch2));

    curl_close($ch2);
     
	//echo $resultado2;
	//echo $requiero;
	
 ?>
