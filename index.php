<?php 
	error_reporting(0);
	include_once 'conexion.php';
	$rfc=$_GET['rfc'];

	$consulta = "SELECT DISTINCT idSolicitudPadron,Folio,WC.idCase,Fecha,Tipomovimiento,Usuario,WI.idTask,T.tskDisplayName,S.Correcto
	FROM SCG.dbo.SolicitudPadron S
	INNER JOIN SCG.dbo.WFCASE WC ON S.Folio = WC.radNumber
	INNER JOIN SCG.dbo.WORKITEM WI ON wC.idCase=WI.idCase
	INNER JOIN SCG.dbo.TASK T ON WI.idTask=T.idTask
	INNER JOIN SCG.dbo.WFUSER WF ON S.Usuario = WF.idUser
	WHERE WF.userName = '$rfc'
	AND WI.idTask=7226
	AND S.Correcto=0";
	//echo $consulta;
	
	$registro = sqlsrv_query($conn,$consulta);
	$results = array();
	while( $row = sqlsrv_fetch_array( $registro)) 
	{
		$results[] = $row;
	}


	$consulta="SELECT idUser FROM WFUSER WHERE userName LIKE '$rfc'";

  $sql=sqlsrv_query($conn,$consulta);
  $values = sqlsrv_fetch_array($sql);

$usern=$values['idUser'];
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
		<meta name="generator" content="Jekyll v3.8.5">
		<title>Ampliación de Factibilidad</title>

		<!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

		<style>
		.bd-placeholder-img {
			font-size: 1.125rem;
			text-anchor: middle;
			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
		}

		@media (min-width: 768px) {
			.bd-placeholder-img-lg {
			font-size: 3.5rem;
			}
		}
		</style>
		<!-- Custom styles for this template -->
		<link rel="stylesheet" href="css/estilo.css"/>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.8.0/sweetalert2.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
		<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet" >
		<script src="js/jquery.js"></script><!--- version 3.5.1 -->
    	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
		
		<script src="js/ajax.js"></script>
		<script src="js/ajaxEditar.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.8.0/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		
		<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
	</head>
<body>
	<!-- <nav class="navbar fixed-top bg-dark flex-md-nowrap p-0 shadow navcea">
	<a class="navbar-brand col-sm-3 col-md-2 mr-0"><img src="img/bizagi-logo.png" alt="" height=40px; width=185px;></a>
	<ul class="navbar-nav px-3">
		<li class="nav-item text-nowrap">
		</li>
	</ul>
	</nav> -->

	<div class="container">
	<div class="row principal">
	<nav class="col-md-2">
	<div class="sidebar-sticky">
		<ul class="nav flex-column">
			<li class="nav-item">
				<a id="btnNuevo" class="btn btn-primary">
					<span data-feather="home"></span>
					<strong><i class="fas fa-plus"> Nueva solicitud</i></strong><span ></span>
				</a>
			</li>
		</ul>
	</div>
	</nav>


	<main role="main"class="col-md-9 ml-sm-auto col-lg-10 px-4">
	<div class="container">
	<div class="table-wrapper">
	<div class="table-title">
		<h1><strong>Bandeja de Entrada</strong></h1>
	</div>
	<div class='clearfix'></div>
	<div id="loader"></div><!-- Carga de datos ajax aqui -->
	<div id="resultados">
		<input type="hidden" id="rfcN" name="rfcN" value="<?php echo $_GET['rfc']; ?>" placeholder="">  
	<hr>
	<table id="tblCasos" class="table table-striped table-bordered" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>Numero Caso</th>
			<th>Folio</th>
			<th>Fecha</th>
			<th>Proceso</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
	<?php  
		foreach($results as $dat) { 
	?> 
		<tr>
			<td><?php echo $dat['idCase']; ?></td>
			<td><?php echo $dat['Folio']; ?></td>
			<td><?php $date=$dat['Fecha']; echo date_format($date, 'Y-m-d'); ?></td>
			<td><?php echo $dat['tskDisplayName']; ?></td>
			<td><button type="button" class="editar btn btn-primary" id="<?php echo $dat['Folio']; ?>"><i class="fa fa-pencil"></i></button></td>
		</tr>
	<?php  
		}
	?>
	</tbody>
	</table>  
	</div>      
	</div>
	</div>    
	</main>

	<!--//////////////////////////////////////////////////MODAL AGREGAR/////////////////////////////////////////-->
	<?php require_once 'obtenerNuevoCaso.php'; ?>
	<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
	<div class="modal-content">
	<div class="modal-header">
		<h5 class="modal-title" id="titulo"><strong></strong></h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<div class="modal-body">
		<h4>1.- Coloca el correo </h4>
	<form id='alta-caso' name='miformulario' method='POST' enctype='multipart/form-data'>
	<div class='form-group row'>
	<div class='form-group col-md-7'>
		<label>Folio:</label>
		<input type='hidden' id='newCaso' name='newCaso' class='form-control' value="<?php obtenerCaso();?>" readonly='readonly' />
		<input type='text' id='radNumber' name='radNumber' class='form-control' value='' readonly='readonly' /> 
		<input type="hidden" id="rfc" name="rfc" value="<?php $rfc = $_GET['rfc']; echo $rfc; ?>">
	</div>
	<!-- <div class='form-group col-md-6'>
		<label>Opción de Trámite:</label>
		<input type='text' id='intramite' name='tramite' value='Ampliación de Factibilidad' class='form-control' readonly='readonly' >
	</div> -->
	<div class='form-group col-md-5'>
		<label>Fecha:</label>
		<div><?php echo fechaSer()?></div>
	</div>
	<div class='form-group col-md-10'>
		<label>Correo electronico para notificar:</label><span class="obligado">*</span>
		<input type='email' id='inEmail' name='correo' class='form-control' placeholder='Ejemplo@sudominio.com' onkeyup='add();'>
		</br>
		<h4>2.- Introduce el numero de expediente</h4>
	</div>
	<!--<form method="post" action=" ?>">-->
	<div class='form-group col-md-5'>
		<label>No_Expediente</label><span class="obligado">*</span>
		 <select id='n_expediente' name='n_expediente' class='c_expediente form-control' multiple>
        	<?php 
            $query="select * from A_Expediente_prod1 where rfc = '$rfc'";
            echo $query;
            $resultado=sqlsrv_query($conn,$query);  
            while($fila = sqlsrv_fetch_array($resultado))
           {
            ?>
             <option value=" <?php echo $fila['noregistro']?>"><?php echo $fila['noregistro']?></option>
            <?php 
            }
           ?>
        </select>
	
	</div>
	<div class='form-group col-md-4'>
		<label>Seleccionar factibilidad:</label><span class="obligado">*</span>
	<div id='select2lista'></div>
	<div id='select3lista'></div>	
	</div>
	<div class='form-group col-md-2'>
		<a tabindex="0" class="btn popover-dismiss" role="button" data-toggle="popover" data-placement="bottom" data-trigger="focus" data-content="En caso de no encontrar su factibilidad favor de comuniarse con el area de sistemas de la CEA"><img src="img/mark_help.png" alt="" srcset=""></a>
	</div>
	<div class='form-group col-md-12'>
		
	<div id='tabla'></div>
	</div>
	
	
	<div class='form-group col-md-12'>
		<h2>Representante Legal</h2>
		<h4>3.- Escriba los siguientes datos del representante legal</h4>
	<table class="table table-hover">
	<thead>
		<tr>
			<th scope="col"><label>Nombre:</label><span class="obligado">*</span></th>
			<th scope="col"><label>Telefono:</label><span class="obligado">*</span></th>
			<th scope="col"><label>Correo:</label><span class="obligado">*</span></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th scope="row"><input type='text' id='nombre' name='nombre' class='form-control' /></th>
			<td><input type='text' id='telefono' name='telefono' class='form-control' /></td>
			<td>  <input type='email' id='correoREP' name='correoREP' class='form-control entrada-usuario' /></td>
			
			
		</tr>
	</tbody>
	</table>
	</div>
	<div class='form-group col-md-12'>
		<h4>4.- Desglose del requerimiento</h4>
		<p>El requerimiento no debe exceder el total del dictamen de uso de suelo.</p>
		
			<table class='table table-hover'>
				<tr>
				<td><label> Doméstico</label><span class="obligado">*</span></td>
				<td> <input type='number' id='reqdomestico' name='reqdomestico' class='form-control amt' oninput='calcular()' value='0' /> </td>
				
				</tr><tr>
				<td><label> Comercial</label><span class="obligado">*</span></td>
				<td> <input type='number' id='reqcomercial' name='reqcomercial' class='form-control amt' oninput='calcular()' value='0' /> </td>
				
				</tr><tr>
				<td><label> Industrial</label><span class="obligado">*</span></td>
				<td> <input type='number' id='reqindustrial' name='reqindustrial' class='form-control' oninput='calcular()' value='0' /> </td>
				
				</tr><tr>
				<td><input type='text' id='reqespecificar' name='reqespecificar' class='form-control'  placeholder='Otro (especificar)'/></td>
				<td> <input type='number' id='reqmixto' name='reqmixto' class='form-control amt'  value='0' oninput='calcular()' required /> </td>
				
				</tr><tr>
				<td><label><b> Total</b></label><span class="obligado">*</span></td>
				<td> <input type='text' id='totalunidades' name='totalunidades' class='form-control' value='0' readonly/> </td>
				
				</tr>
				</table>
	</div>
	<div class='form-group col-md-12'>
		<h4>5.- Seleccione los siguientes archivos desde su computadora.</h4>
	<table class="table table-hover">
	<thead>
		<tr>
			<th scope="col">Documento</th>
			<th scope="col">Anexo</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th scope="row"><label>Oficio de Respuesta de factibilidad vigente</label><span class="obligado">*</span></th>
			<td>
				
				<input type="file" id="filePicker" name="archivo" class="form-control inputfile inputfile-7"  accept=".pdf"  />
                <label for="filePicker">
                <span class="iborrainputfile" id="archivo"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                </label>
				<input type='hidden' id='base64textarea' name='code1' class='form-control' />
			</td>
		</tr>
		<tr>
			<th scope="row"><label>Informe Uso Suelo</label><span class="obligado">*</span></th>
			<td colspan="2">
				<input type="file" id="filePicker2" name="archivo2" class="form-control inputfile inputfile-7"  accept=".pdf"  />
                <label for="filePicker2">
                <span class="iborrainputfile" id="archivo2"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                </label>
				<input type='hidden' id='base64textarea2' name='code2' class='form-control' />
			</td>
		</tr>

		<tr>
			<th scope="row"><label>Croquis del predio de google earth</label><span class="obligado">*</span></th>
			<td colspan="2">
				<input type="file" id="filePicker3" name="archivo3" class="form-control inputfile inputfile-7"  accept=".pdf"  />
                <label for="filePicker3">
                <span class="iborrainputfile" id="archivo3"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                </label>
				<input type='hidden' id='base64textarea3' name='code3' class='form-control' />
			</td>
		</tr>

		<tr>
			<th scope="row"><label>Oficio Solicitud</label><span class="obligado">*</span></th>
			<td colspan="2">
				<input type="file" id="filePicker4" name="archivo4" class="form-control inputfile inputfile-7"  accept=".pdf" />
                <label for="filePicker4">
                <span class="iborrainputfile" id="archivo4"><i class="fas fa-upload"></i>Seleccionar archivo</span>
                </label>
				<input type='hidden' id='base64textarea4' name='code4' class='form-control' />
			</td>
		</tr>

		</tbody>
		</table>   
	</div>

	</div>          
	</div>
	<div class="modal-footer">
	<input type="hidden" id="vacio" name="vacio">
	<div id="precarga"></div>
		<input type='submit' id='btn_enviar' name='creaCaso' value='Enviar Solicitud' class='btn btn-primary' />
		<!--<input type='button' id='guardar' name='guarda_btn' value='GrabarDatos' class='btn btn-secondary' onclick='realizaProcesoGuardar()'/>-->
		<input type='hidden' id='transaccion'name='transaccion' value='insertar'>
	</div>
	</div>
	</div>
	</div>
	</div>
	<!--//////////////////////////////////////////////////MODAL EDITAR/////////////////////////////////////////-->
	<?php 
		require_once 'editar.php';
	
	 ?>
	
	<!--modal editar-->

	<script>
		$('.popover-dismiss').popover({
			trigger: 'focus'
		})
	</script>
	
	<script>
		var handleFileSelect = function(evt) {
		var files = evt.target.files;
		var file = files[0];

		if (files && file) {
			var reader = new FileReader();

		reader.onload = function(readerEvt) {
			var binaryString = readerEvt.target.result;
			document.getElementById("base64textarea").value = btoa(binaryString);
		};

			reader.readAsBinaryString(file);
		}
		};

		if (window.File && window.FileReader && window.FileList && window.Blob) {
			document.getElementById('filePicker').addEventListener('change', handleFileSelect, false);
		} else {
			alert('The File APIs are not fully supported in this browser.');
		}
	</script>   

	<script>
		var handleFileSelect = function(evt) {
		var files = evt.target.files;
		var file = files[0];

		if (files && file) {
			var reader = new FileReader();

		reader.onload = function(readerEvt) {
			var binaryString = readerEvt.target.result;
			document.getElementById("base64textarea2").value = btoa(binaryString);
		};
			reader.readAsBinaryString(file);
		}
		};

		if (window.File && window.FileReader && window.FileList && window.Blob) {
			document.getElementById('filePicker2').addEventListener('change', handleFileSelect, false);
		} else {
			alert('The File APIs are not fully supported in this browser.');
		}
	</script>   

	<script>
		var handleFileSelect = function(evt) {
		var files = evt.target.files;
		var file = files[0];

		if (files && file) {
			var reader = new FileReader();

		reader.onload = function(readerEvt) {
			var binaryString = readerEvt.target.result;
		document.getElementById("base64textarea3").value = btoa(binaryString);
		};

			reader.readAsBinaryString(file);
		}
		};

		if (window.File && window.FileReader && window.FileList && window.Blob) {
			document.getElementById('filePicker3').addEventListener('change', handleFileSelect, false);
		} else {
			alert('The File APIs are not fully supported in this browser.');
		}
	</script>  

	<script>
		var handleFileSelect = function(evt) {
		var files = evt.target.files;
		var file = files[0];

		if (files && file) {
			var reader = new FileReader();

		reader.onload = function(readerEvt) {
			var binaryString = readerEvt.target.result;
		document.getElementById("base64textarea4").value = btoa(binaryString);
		};

			reader.readAsBinaryString(file);
		}
		};

		if (window.File && window.FileReader && window.FileList && window.Blob) {
			document.getElementById('filePicker4').addEventListener('change', handleFileSelect, false);
		} else {
			alert('The File APIs are not fully supported in this browser.');
		}
	</script>  
	
	<script>
		function add()
		{
			var elements = document.querySelectorAll(".entrada-usuario");
			var valor = document.getElementById('inEmail').value;

		for(var i = 0; i < elements.length;i++)
		{
			elements[i].value = valor;
		}
		}
	</script>
	<!-- scripts para modal editar-->
	
	
	
</body>
</html>        