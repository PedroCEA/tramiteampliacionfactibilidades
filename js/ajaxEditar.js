$(document).on('click','.editar',function(){
    var edit_id=$(this).attr('id');
    $.ajax({
        url:"edit_data.php",
        type:"post",
        data:{edit_id:edit_id},
        success:function(data){
            //console.log(data);
            $("#info-update").html(data);
            $(".modal-header").css('background-color','#3393FF'); 
            $(".modal-header").css('color','white'); 
            $(".modal-title").text(" Editar Solicitud Ampliación de Factibilidad");
            $("#editarModal").modal('show');
        }
    });

});


function realizaProcesoEditar(){
    let url = "llamarWebServiceEditar.php"; // El script a dónde se realizará la petición.

    const form = document.getElementById('edita-caso');
    const paqueteDeDatos = new FormData(form);

    $.ajax({
        
        beforeSend: function(){
            $('#precarga').html('<span>Un momento por favor ...</span>');
        },

        type: "POST",
        url: url,
        contentType: false,
        data: paqueteDeDatos, // Adjuntar los campos del formulario enviado.
        processData: false,
        cache: false, 
        dataType: "html",  // Adjuntar los campos del formulario enviado.
        success: function(data)
        {
                
        $('#precarga').hide(1000);
        //$("#precarga").html(data); // Mostrar la respuestas del script PHP.
       //console.log(data);

        },
        complete: function()
        {
        var folio = $('#numRadNumber').val();
        swal({
            title: 'Buen trabajo!',
            text: "Tu trámite ha sido enviada con el folio:"+folio,
            type: 'success',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'OK!'
          }).then((result) => {
            if (result.value) {
              window.location.reload();
            }
          })
      
       
     }         
        //$("#respuesta").hide(3500);
   
    
     
   });
   //
   
return false; // Evitar ejecutar el submit del formulario.
   

}
