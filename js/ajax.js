$(document).ready(function(){

   $("input").focus(function(){
    $('.alert').remove();
    colorDefault('inEmail');
    colorDefault('nombre');
    colorDefault('telefono');
    colorDefault('correoREP');
    colorDefault('reqdomestico');
    colorDefault('reqcomercial');
    colorDefault('reqindustrial');
    colorDefault('totalunidades');
    colorDefault('filePicker');
    colorDefault('filePicker2');
    colorDefault('filePicker3');
    colorDefault('filePicker4');
    
});
$("select").focus(function(){
  $('.alert').remove();
  colorDefault('n_expediente');
  colorDefault('factibilidades_slc');
});

    tablaCasos=$("#tblCasos").DataTable({
        // "columnDefs":[{
        //     "targets":-1,
        //     "data":null,
        //     "defaultContent":"<button type='button' class='editar btn btn-primary'><i class='fa fa-pencil'></i></button>"
        // }],
        "language": {
    "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad"
                }
            }
    });

$("#btnNuevo").click(function(){
  $("#alta-caso").trigger("reset");
  //Creamos el caso
     var url = "creaCaso.php"; // El script a dónde se realizará la petición.
      var rfc = $('#rfcN').val();
      $.ajax({
        type: "POST",
        url:url,
        data: {"rfc":rfc},
        success: function(r)
            {
              console.log(r);
              $('#radNumber').val(r);
                
               
            },
    });

      
   $(".modal-header").css('background-color','#3393FF'); 
   $(".modal-header").css('color','white'); 
   $(".modal-title").text("Nueva Ampliación de Factibilidad");
  
    $('#addModal').modal({ backdrop: 'static', keyboard: false });
    $('#addModal').modal('show');
});


 // $(".editar").click(function(){
 //   //$("#alta-caso").trigger("reset");
 //   $(".modal-header").css('background-color','#3393FF'); 
 //   $(".modal-header").css('color','white'); 
 //   $(".modal-title").text(" Editar Ampliación de Factibilidad");
 //    fila = $(this).closest("tr");
   
 //    id = parseInt(fila.find('td:eq(0)').text());
 //    radnumber = fila.find('td:eq(1)').text();
    
 //    var midVal = $('#numCaso').val(id);
 //    var midValRad = $('#numRadNumber').val(radnumber);
    
 //     obtenerForm();
 //    $("#editarModal").modal('show');
   
 // });

 $('.c_expediente').change(function(){
    recargarLista();
 });

 $( '.inputfile' ).each( function()
  {
    let $input   = $( this ),
      $label   = $input.next( 'label' ),
      labelVal = $label.html();

    $input.on( 'change', function( e )
    {
      let fileName = '';

      if( this.files && this.files.length > 1 ){
        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
      }
      else if( e.target.value ){
        fileName = e.target.value.split( '\\' ).pop();
      }

      if( fileName ){
        $label.find( 'span' ).html( fileName );

      }
      else{
        $label.html( labelVal );
      }

    });// fin change
  });//fin each

$(function(){
        $("#alta-caso").on("submit", function(e){
            e.preventDefault();
            $('.alert').remove();
             let correo = $("#inEmail").val(),
                 expediente = $("#n_expediente").val(),
                 factibilidad = $("#factibilidades_slc").val(),              
                 nombre = $("#nombre").val();
                 telefono = $("#telefono").val();
                 correoREP = $("#correoREP").val();
                 reqdomestico = $("#reqdomestico").val();
                 reqcomercial = $("#reqcomercial").val();
                 reqindustrial = $("#reqindustrial").val();
                 reqespecificar = $("#reqespecificar").val();
                 reqmixto = $("#reqmixto").val();
                 totalunidades = $("#totalunidades").val();

                 archivo1 = $("#filePicker").val();
                 archivo2 = $("#filePicker2").val();
                 archivo3 = $("#filePicker3").val();
                 archivo4 = $("#filePicker4").val();



            if (correo == "" || correo == null) {
                cambiarColor("inEmail");
                //mostramos el mensaje de alerta
                mostarAlerta("Por favor escribe un Correo");
                return false;
            }
            if (expediente == "" || expediente == 0) {
                cambiarColor("n_expediente");
                //mostramos el mensaje de alerta
                mostarAlerta("Por favor selecciona un expediente");
                return false;
            }
            if (factibilidad == "" || factibilidad == 0) {
                cambiarColor("factibilidades_slc");
                //mostramos el mensaje de alerta
                mostarAlerta("Por favor selecciona la factibilidad a tramitar");
                return false;
            }
            if (nombre == "" || nombre == null) {
                cambiarColor("nombre");
                //mostramos el mensaje de alerta
                mostarAlerta("Por favor escribe el nombre del representante");
                return false;
            }
            if (telefono == "" || telefono == null) {
                cambiarColor("telefono");
                //mostramos el mensaje de alerta
                mostarAlerta("Por favor escribe el telefono del representante");
                return false;
            }
            if (correoREP == "" || correoREP == null) {
                cambiarColor("correoREP");
                //mostramos el mensaje de alerta
                mostarAlerta("Por favor escribe el Correo del representante");
                return false;
            }
            if (reqdomestico == "" || reqdomestico == null) {
                cambiarColor("reqdomestico");
                //mostramos el mensaje de alerta
                mostarAlerta("Por favor escribe las unidades domesticas requeridas.");
                return false;
            }
            if (reqcomercial == "" || reqcomercial == null) {
                cambiarColor("reqcomercial");
                //mostramos el mensaje de alerta
                mostarAlerta("Por favor escribe las unidades comerciales requeridas.");
                return false;
            }
            if (reqindustrial == "" || reqindustrial == null) {
                cambiarColor("reqindustrial");
                //mostramos el mensaje de alerta
                mostarAlerta("Por favor escribe las unidades industriales requeridas.");
                return false;
            }
            if (totalunidades == "" || totalunidades == 0) {
                cambiarColor("totalunidades");
                //mostramos el mensaje de alerta
                mostarAlerta("el total debe ser mayor a cero.");
                return false;
            }
            if (archivo1 == "" || archivo1 == null) {
                cambiarColor("filePicker");
                //mostramos el mensaje de alerta
                mostarAlerta("Por favor revisé el Oficio de Respuesta de factibilidad vigente puede que no este cargado o no tenga una extension valida..");
                return false;
            }
            if (archivo2 == "" || archivo2 == null) {
                cambiarColor("filePicker2");
                //mostramos el mensaje de alerta
                mostarAlerta("Por favor revisé el Informe Uso Suelo puede que no este cargado o no tenga una extension valida.");
                return false;
            }
            if (archivo3 == "" || archivo3 == null) {
                cambiarColor("filePicker3");
                //mostramos el mensaje de alerta
                mostarAlerta("Por favor revisé el Croquis del predio de google earth puede que no este cargado o no tenga una extension valida.");
                return false;
            }
            if (archivo4 == "" || archivo4 == null) {
                cambiarColor("filePicker4");
                //mostramos el mensaje de alerta
                mostarAlerta("Por favor revisé el Oficio Soicitud puede que no este cargado o no tenga una extension valida.");
                return false;
            }

            const form = document.getElementById('alta-caso');
            const paqueteDeDatos = new FormData(form);
            //console.log(paqueteDeDatos);


            let destino = "llamarWebService.php";
            $.ajax({
            beforeSend: function(){
            $('#precarga').html('<p>Un momento por favor ...</p>');
            },
            url: destino,
            type: 'POST', // Siempre que se envíen ficheros, por POST, no por GET.
            contentType: false,
            data: paqueteDeDatos, // Al atributo data se le asigna el objeto FormData.
            processData: false,
            cache: false, 
            success: function(resultado){ // En caso de que todo salga bien.
              console.log(resultado);
              $('#precarga').hide(1000);

              let folio = $('#radNumber').val();
              if (resultado != "") {
                //console.log("entro al if");
                swal({
                    title: 'Buen trabajo!',
                    text: "Tu trámite ha sido enviada con el folio:"+folio,
                    type: 'success',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK!'
                  }).then((result) => {
                    if (result.value) {
                      window.location.reload();
                    }
                  })
              }else if(resultado == "0"){
                //console.log("entro al else");
                    swal({
                        title: 'Alerta, solicitud no enviada',
                        text: "Su solicitud con número de folio:"+folio+" no se envió correctamente,le sugerimos intentarlo nuevamente, si el error persiste le recomendamos utilizar la opción 2 o comunicarse a la extensión 1011 ó 1012 en horario de lunes a viernes de 8:00 a 17:00 hrs.",
                        type: 'error',
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'OK!'
                    }).then((result) => {
                        if (result.value) {
                            //window.location.reload();
                        }
                    }) 
              }
                
                
            },
            
            error: function (){ // Si hay algún error.
              alert("Algo ha fallado intentelo nuevamente");
            }
          });//fin ajax
       
           
        });// fin submit 
    });//fin function 

});

 function obtenerForm(){
        selectRadNumber= $("#numRadNumber").val();
        selectIdCase=$("#numCaso").val();

     //alert(selectIdCase);
    $.ajax({
        type: "POST",
        url:"obtenerDatosEditar.php",
        data: {"selectRadNumber":selectRadNumber,"selectIdCase":selectIdCase},
        success: function(r)
            {
                $("#editarForm").html(r);
               
            },
    });
}


// function realizaProceso(){
//     var email = $("#inEmail").val();
//     var expediente = $("#n_expediente").val();
//     var factibilidad = $("#factibilidades_slc").val();
//     var archivo1 = $("#filePicker").val();
//     var archivo2 = $("#filePicker2").val();
//     var archivo3 = $("#filePicker3").val();
//     var archivo4 = $("#filePicker4").val();
//     var nombre = $("#nombre").val();
//     var telefono = $("#telefono").val();
//     var correoREP = $("#correoREP").val();
//     var SeleccionadoREP = $("#SeleccionadoREP").val();
//     var ExisteexpedienteOR = $("#ExisteexpedienteOR").val();
//     var reqdomestico = $("#reqdomestico").val();
//     var reqcomercial = $("#reqcomercial").val();
//     var reqindustrial = $("#reqindustrial").val();
//     var reqespecificar = $("#reqespecificar").val();
//     var reqmixto = $("#reqmixto").val();
//     var totalunidades = $("#totalunidades").val();
   
//     if (email == "") 
//     {
//         toastr.error("No ha ingresado correo","Aviso!");
//         return false;    
//     }
   
//     if (expediente == "") {
//         toastr.error("No ha ingresado un numero de expediente","Aviso!");
//         return false;   
//     }
//     if (factibilidad == "") {
//         toastr.error("No ha ingresado un numero de factibilidad","Aviso!");
//         return false;   
//     }
//     if (archivo1 == "") {
//         toastr.error("No has cargado el FORMATO DE SOLICITUD DEL DESARROLLADOR","Aviso!");
//         return false;   
//     }
//     if (nombre == "") {
//         toastr.error("No ha ingresado el nombre del Representante","Aviso!");
//         return false;   
//     }
//     if (telefono == "") {
//         toastr.error("No ha ingresado el telefono del Representante","Aviso!");
//         return false;   
//     }
//     if (correoREP == "") {
//         toastr.error("No ha ingresado el correo del Representante","Aviso!");
//         return false;   
//     }
// 	if (SeleccionadoREP== "") {
//         toastr.error("No ha seleccionado el Representante","Aviso!");
//         return false;   
//     }
// 	if (ExisteexpedienteOR == "") {
//         toastr.error("No ha Confirmado la factibilidad","Aviso!");
//         return false;   
//     }
// 	if (reqdomestico == "") {
//         toastr.error("No ha ingresado el requerimiento domestico","Aviso!");
//         return false;   
//     }
// 	if (reqcomercial == "") {
//         toastr.error("No ha ingresado el requerimiento comercial","Aviso!");
//         return false;   
//     }
// 	if (reqindustrial == "") {
// 		toastr.error("No ha ingresado el requerimiento industrial","Aviso!");
//         return false;   
//     }
// 	if (reqmixto == "") {
//         toastr.error("No ha ingresado el requerimiento para el especifico","Aviso!");
//         return false;   
//     }
// 	if (totalunidades == "") {
//         toastr.error("No ha ingresado el total de unidades","Aviso!");
//         return false;   
//     }
//    var url = "llamarWebService.php"; // El script a dónde se realizará la petición.
//       $.ajax({
//         beforeSend: function(){
//             $('#precarga').html('<span>Un momento por favor ...</span>');
//         },
//              type: "POST",
//              url: url,
//              data: $("#alta-caso").serialize(), // Adjuntar los campos del formulario enviado.
//              success: function(data)
//              {
//                 console.log(data);
//                 $('#precarga').hide(1000);
                
//                //alert(data);
//                 //$("#respuesta").html(data); // Mostrar la respuestas del script PHP.
              
//              },
//              complete: function()
//              {
//                  var folio = $('#radNumber').val();
//                 swal({
//                     title: 'Buen trabajo!',
//                     text: "Tu trámite ha sido enviada con el folio:"+folio,
//                     type: 'success',
//                     confirmButtonColor: '#3085d6',
//                     confirmButtonText: 'OK!'
//                   }).then((result) => {
//                     if (result.value) {
//                       window.location.reload();
//                     }
//                   })
              
                
//              }         
//                 //$("#respuesta").hide(3500);
           
            
             
//            });
//            //
           
//         return false; // Evitar ejecutar el submit del formulario.
           
// }

// function realizaProcesoEditar(){
//     var url = "llamarWebServiceEditar.php"; // El script a dónde se realizará la petición.
//     var numCaso=$("#numCaso").val();
//     var n_expediente=$("#n_expediente").val();
//     var numRadNumber=$("#numRadNumber").val();
//     var num_fac=$("#num_fac").val();
//     var username=$("#edrfc").val();
//     var numfact=$("#num_fac").val();
//     var edfichero=$("#edfichero").val();
//     var code1=$("#edbase64textarea").val();
//     var edfichero2=$("#edfichero2").val();
//     var code2=$("#edbase64textarea2").val();
//     var edfichero3=$("#edfichero3").val();
//     var code3=$("#edbase64textarea3").val();
//     var nombreeditar=$("#nombreeditar").val();
//     var telefonoeditar=$("#telefonoeditar").val();
//     var correoREPeditar=$("#correoREPeditar").val();
//     var SeleccionadoREPeditar=$("#SeleccionadoREPeditar").val();
//     var Existeexpediente=$("#Existeexpediente").val();
//     var edreqdomestico=$("#edreqdomestico").val();
//     var edreqcomercial=$("#edreqcomercial").val();
//     var edreqindustrial=$("#edreqindustrial").val();
//     var edreqespecificar=$("#edreqespecificar").val();
//     var edreqmixto=$("#edreqmixto").val();
//     var edtotalunidades=$("#edtotalunidades").val();
// 	var rfc=$("#rfc").val();
//     //alert(nombreeditar);
//     var param={
//                  'numCaso':numCaso,
//                  'numRadNumber':numRadNumber,
//                  'n_expediente':n_expediente,
//                  'num_fac':num_fac,
//                  'username':username,
//                  'numfact':numfact,
//                  'edfichero':edfichero,
//                  'code1':code1,
//                  'edfichero2':edfichero2,
// 				 'code2':code2,
//                  'edfichero3':edfichero3,
//                  'code3':code3,
//                  'nombreeditar':nombreeditar,
//                  'telefonoeditar':telefonoeditar,
//                  'correoREPeditar':correoREPeditar,
//                  'SeleccionadoREPeditar':SeleccionadoREPeditar,
// 				 'Existeexpediente':Existeexpediente,
// 				 'edreqdomestico':edreqdomestico,
// 				 'edreqcomercial':edreqcomercial,
// 				 'edreqindustrial':edreqindustrial,
// 				 'edreqespecificar':edreqespecificar,
// 				 'edreqmixto':edreqmixto,
// 				 'edtotalunidades':edtotalunidades,
// 				 'rfc':rfc
//     }
   

//       $.ajax({
        
//         beforeSend: function(){
//             $('#precarga').html('<img id="loader" src="../img/loading.gif" />');
//         },

//              type: "POST",
//              url: url,
//              data: param,
//              dataType: "html", // Adjuntar los campos del formulario enviado.
//              success: function(data)
//              {
                
//                 $('#precarga').hide(1000);
//                 //$("#precarga").html(data); // Mostrar la respuestas del script PHP.
//                console.log(data);

//              },
//              complete: function()
//              {
//                 var folio = $('#numRadNumber').val();
//                 swal({
//                     title: 'Buen trabajo!',
//                     text: "Tu trámite ha sido enviada con el folio:"+folio,
//                     type: 'success',
//                     confirmButtonColor: '#3085d6',
//                     confirmButtonText: 'OK!'
//                   }).then((result) => {
//                     if (result.value) {
//                       window.location.reload();
//                     }
//                   })
              
               
//              }         
//                 //$("#respuesta").hide(3500);
           
            
             
//            });
//            //
           
//         return false; // Evitar ejecutar el submit del formulario.
           

// }

function recargarLista(){
    $.ajax({
        type: "POST",
        url: "obtenerFactibilidad.php",
        data: $("#alta-caso").serialize(),
        success:function(r){
            $('#select2lista').html(r);
        }
    });
}    

function mostrarRelacion()
{
    var url = "mostrarDetalleFactibilidad.php"; // El script a dónde se realizará la petición.
    $.ajax({
           type: "POST",
           url: url,
           data: $("#alta-caso").serialize(), // Adjuntar los campos del formulario enviado.
           success: function(data)
           {
               $("#tabla").html(data); // Mostrar la respuestas del script PHP.
           },

           complete: function()
           {
             // $("#respuesta").hide(3500);
           }
          
         });
	// $.ajax({
 //        type: "POST",
 //        url: "obtenerRequerimientos.php",
 //        data: $("#alta-caso").serialize(),
 //        success:function(r){
 //            $('#selectrequerimientos').html(r);
 //        }
 //    });
         
    return false; // Evitar ejecutar e
}

function calcular() {
    
        var dom = parseInt(document.getElementById("reqdomestico").value) || 0,
            com = parseInt(document.getElementById("reqcomercial").value) || 0,
            ind = parseInt(document.getElementById("reqindustrial").value) || 0,
            mix = parseInt(document.getElementById("reqmixto").value) || 0;
            
            document.getElementById("totalunidades").value = dom + com + ind + mix;
   
    
}

function colorDefault(dato){
    $('#' + dato).css({
        border: "1px solid #999"

    });
}

//funcion para cambiar el color de borde
function cambiarColor(dato){
    $('#'+dato).css({
        borderColor:"red"
    });    
}

//funcion para mostrar la alerta
function mostarAlerta(texto){
    $("#vacio").before('<div class="alert alert-warning" role="alert">'+ texto +'</div>');
}


$(document).on('change','input[type="file"]',function(){
    // this.files[0].size recupera el tamaño del archivo
    // alert(this.files[0].size);
    
    var fileName = this.files[0].name;
    var fileSize = this.files[0].size;

    if(fileSize > 5000000){
        $("#vacio").before('<div class="alert alert-warning" role="alert">El archivo no debe superar los 5MB</div>');
        //mostrarAlerta('El archivo no debe superar los 5MB');
        this.value = '';
        this.files[0].name = '';
    }else{
        // recuperamos la extensión del archivo
        var ext = fileName.split('.').pop();
        
        // Convertimos en minúscula porque 
        // la extensión del archivo puede estar en mayúscula
        ext = ext.toLowerCase();
    
        //console.log(ext);
        switch (ext) {
            case 'jpg':
            case 'kmz':
            case 'kml':
            case 'pdf': break;
            default:
                mostarAlerta('El archivo no tiene la extensión adecuada')
                this.value = ''; // reset del valor
                this.files[0].name = '';
        }
    }
});
