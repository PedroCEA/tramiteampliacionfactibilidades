<?php 

require_once 'conexion.php';

if(isset($_POST['edit_id'])){
	$folio=$_POST['edit_id'];

$obCase="select * from wfcase where radNumber='$folio'";	

 $sql1=sqlsrv_query($conn,$obCase);
  $result = sqlsrv_fetch_array($sql1);
 

$caso=$result['idCase'];

$consulta="select top 1 ex.idExpediente,ex.Noexpediente,af.No_Factibilidad, sp.Folio,sp.Usuario,sp.Tipotramite,sp.Tipomovimiento,sp.Correo,sp.Fecha,sp.Serepresenta,
sp.TipoRepresentante,ex.Cvescatastrales,ex.Desarrollo,ex.Cvecatastral,ex.Existefideicomiso,ex.Copropiedad,ex.Superficie,ex.Tipovivienda,ex.Regimenconstruccion,ex.Domicilio,ex.Colonia,
ex.Municipio,ex.CP,ex.Telefono,ex.Correo as correopr,ex.Reqdomestico,ex.Reqcomercial,ex.Reqindustrial,ex.Reqmixto,ex.Totalunidades,
c.Nodocumento,c.Tipodocumento,r.Nombre,r.Telefono as telefonorep,r.Correo as correorep,r.Seleccionado
from Expediente ex 
inner join SolicitudPadron sp on ex.SolicitudPadron=sp.idSolicitudPadron
inner join A_Factibilidades af on af.No_Expediente=ex.Noexpediente 
inner join Cedula c on ex.Cedula=c.idCedula
inner join Representantes r on ex.idExpediente=r.Expediente
where sp.Folio='$folio'";

//echo $consulta;

$sql=sqlsrv_query($conn,$consulta);
  
$values = sqlsrv_fetch_array($sql);

$folioed=$values['Folio'];
$fechae=$values['Fecha']->format('Y-m-d');
$correoe=$values['Correo'];
$noExpediente=$values['Noexpediente'];
$numFactibili=$values['No_Factibilidad'];
$nombreRe=$values['Nombre'];
$telefenoRe=$values[33];
$correoRe=$values[34];
$reqdomestico=$values['Reqdomestico'];
$reqcomercial=$values['Reqcomercial'];
$reqindustrial=$values['Reqindustrial'];
$reqmixto=$values['Reqmixto'];
$Totalunidades=$values['Totalunidades'];
$usuario=$values['Usuario'];
  

$consulta="SELECT userName FROM WFUSER WHERE idUser LIKE '$usuario'";

$sql=sqlsrv_query($conn,$consulta);
$values = sqlsrv_fetch_array($sql);

$userrfc=$values['userName'];
    
}


$consultaObservaciones="SELECT TOP 1 Observacion FROM Observaciones WHERE Usuario = $usuario order by idObservaciones desc";

$sqlOb=sqlsrv_query($conn,$consultaObservaciones);
$valuesOb = sqlsrv_fetch_array($sqlOb);
$obser=$valuesOb['Observacion'];

?>

<h4>1.- Coloca el correo </h4>
<form id='edita-caso' name='miformulario' method='POST' enctype='multipart/form-data'>
<div class='form-group row'>
<div class='form-group col-md-6'>
<label>Folio:</label>
<input type='hidden' id='numCaso' name='numCaso' class='form-control' readonly='readonly' value="<?php echo $caso; ?>"/>
<input type="hidden" id="rfced" name="rfced" value="<?php echo $userrfc; ?>">
<input type='text' id='numRadNumber' name='numRadNumber' class='form-control' readonly='readonly' value="<?php echo $folioed; ?>"/> 
</div>

<div class='form-group col-md-6'>
<label>Fecha:</label>
<input type='text' id='editFecha' name='editFecha' class='form-control' value="<?php echo $fechae; ?>" readonly='readonly'>
</div>
<div class='form-group col-md-12'>
<label>Observaciones</label>

<textarea name='observa' class='form-control' readonly='readonly'><?php echo $obser ?></textarea>
</div>
<div class='form-group col-md-12'>
<label>Correo electronico para notificar:</label>
<input type='email' id='inEmail' name='correo' class='form-control' required='required' value="<?php echo $correoe; ?>" onkeyup='add();'readonly='readonly'>
</br>
<h4>2.- Introduce el numero de expediente</h4>
</div>
<div class='col-md-6'>
<label>No_Expediente</label>
<div>
<input type='text' id='n_expediente' name='n_expediente' class='form-control' value='<?php echo $noExpediente;?>' aria-label='# de Expediente' aria-describedby='button-addon2' readonly='readonly'>
<div class='input-group-append'>
</div>
</div>
</div>
<div class='col-md-6'>
<label>Seleccionar factibilidad:</label>
<div id='select2lista'><input type='text' id='num_fac' name='num_fac' value='<?php echo $numFactibili; ?>' class='form-control'readonly='readonly'></div>
<div id='select3lista'></div>
</div>
<hr>
<div class='col-md-12'>
<h2>Representante Legal</h2>
<h4>3.- Escriba los siguientes datos del representante legal</h4>
<table class='table table-hover'>
<thead>
<tr>
    <th><label>Nombre:</label></th>
    <th><label>Telefono:</label></th>
    <th><label>Correo:</label></th>
</tr>
</thead>
<tbody>
<tr>
    <th><input type='text' id='nombreeditar' name='nombreeditar' class='form-control' value='<?php echo $nombreRe; ?>' /></th>
    <td><input type='text' id='telefonoeditar' name='telefonoeditar' class='form-control' value='<?php echo $telefenoRe; ?>' /></td>
    <td><input type='email' id='correoREPeditar' name='correoREPeditar' class='form-control entrada-usuario' value='<?php echo $correoRe ?>'/></td>
</tr>
</tbody>
</table>
</div>
</div>

<div class='col-md-12'>
<h4>4.- Desglose de requerimiento</h4>
<table class='table table-hover'>
<tr>
<td><label>Doméstico</label></td>
<td><input type='text' id='edreqdomestico' name='edreqdomestico' class='form-control' onBlur='sumar()' value='<?php echo $reqdomestico; ?>' /></td>
</tr>
<tr>
<td><label>Comercial</label></td>
<td><input type='text' id='edreqcomercial' name='edreqcomercial' class='form-control' onBlur='sumar()' value='<?php echo $reqcomercial; ?>' /></td>
</tr>
<tr>
<td><label>Industrial</label></td>
<td><input type='text' id='edreqindustrial' name='edreqindustrial' class='form-control' onBlur='sumar()' value='<?php echo $reqindustrial; ?>' /></td>
</tr>
<tr>
<td><input type='text' id='edreqespecificar' name='edreqespecificar' class='form-control' required placeholder='Otro' /></td>
<td><input type='text' id='edreqmixto' name='edreqmixto' class='form-control' onBlur='sumar()' value='<?php echo $reqmixto; ?>' /></td>
</tr>
<tr>
<td><label>Total</label></td>
<td><input type='text' id='edtotalunidades' name='edtotalunidades' class='form-control' value='<?php echo $Totalunidades; ?>' readonly='readonly'/></td>
</tr>
</table>
</div>
    
    <div class='col-md-12'>
        <h4>5.- Seleccione los siguientes archivos desde su computadora.</h4>
    <table class='table table-hover'>
    <thead>
        <tr>  
            <th scope='col'>Documento</th>
            <th scope='col'>Anexo</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th scope='row'><label>Oficio de Respuesta de factibilidad vigente</label></th>
            <td><input type="file" id="edarchivo" name="edarchivo" class="form-control inputfile inputfile-7" accept=".pdf">
            <label for="edarchivo">
                <span class="iborrainputfile" id="archivoe"><i class="fas fa-upload"></i>Seleccionar archivo</span>
            </label>
            <input type="hidden" id="edbase64textarea" name="edbase64textarea" class='form-control' /></td>
        </tr>
        <tr>    
            <th><label>Informe Uso Suelo</label></th>
            <td><input type='file' id='edarchivo1' name='edarchivo1' class='form-control inputfile inputfile-7' accept=".pdf">
            <label for="edarchivo1">
                <span class="iborrainputfile" id="archivo1e"><i class="fas fa-upload"></i>Seleccionar archivo</span>
            </label>
            <input type='hidden' id='edbase64textarea1' name='edbase64textarea1' class='form-control' /></td>
        </tr>
        <tr>
            <th><label>Croquis del predio de google earth</label></th>
            <td><input type="file" id="edarchivo2" name="edarchivo2" class="form-control inputfile inputfile-7" accept=".pdf">
            <label for="edarchivo2">
                <span class="iborrainputfile" id="archivo2e"><i class="fas fa-upload"></i>Seleccionar archivo</span>
            </label>
            <input type='hidden' id='edbase64textarea2' name='edbase64textarea2' class='form-control' /></td>
        </tr>
        <tr>
            <th><label>Oficio Solicitud</label></th>
            <td><input type="file" id="edarchivo3" name="edarchivo3" class="form-control inputfile inputfile-7" accept=".pdf">
            <label for="edarchivo3">
                <span class="iborrainputfile" id="archivo3e"><i class="fas fa-upload"></i>Seleccionar archivo</span>
            </label>
            <input type='hidden' id='edbase64textarea3' name='edbase64textarea3' class='form-control' /></td>
        </tr>
    </tbody>
    </table>   
    </div>
    </div>
    <div class='modal-footer'>
    <div id='precarga'></div>
        <input type='button' id='btn_enviar_ed' name='creaCaso' value='Enviar Solicitud' class='btn btn-primary' onclick='realizaProcesoEditar()'/>
    </div>
    </div>
    </div>
    </div>
    </form>

<script>
$(document).ready(function(){

    $( '.inputfile' ).each( function(){
        let $input   = $( this ),
        $label   = $input.next( 'label' ),
        labelVal = $label.html();

        $input.on( 'change', function( e ){
            let fileName = '';

            if( this.files && this.files.length > 1 ){
                fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
            }
            else if( e.target.value ){
                fileName = e.target.value.split( '\\' ).pop();
            }

            if( fileName ){
                $label.find( 'span' ).html( fileName );

            }
            else{
                $label.html( labelVal );
            }

        });// fin change
    });//fin each
});
</script>

<script>
        var handleFileSelect = function(evt) {
        var files = evt.target.files;
        var file = files[0];

        if (files && file) {
            var reader = new FileReader();

        reader.onload = function(readerEvt) {
            var binaryString = readerEvt.target.result;
            document.getElementById("edbase64textarea").value = btoa(binaryString);
        };

            reader.readAsBinaryString(file);
        }
        };

        if (window.File && window.FileReader && window.FileList && window.Blob) {
            document.getElementById('edarchivo').addEventListener('change', handleFileSelect, false);
        } else {
            alert('The File APIs are not fully supported in this browser.');
        }
    </script>   

    <script>
        var handleFileSelect = function(evt) {
        var files = evt.target.files;
        var file = files[0];

        if (files && file) {
            var reader = new FileReader();

        reader.onload = function(readerEvt) {
            var binaryString = readerEvt.target.result;
            document.getElementById("edbase64textarea1").value = btoa(binaryString);
        };
            reader.readAsBinaryString(file);
        }
        };

        if (window.File && window.FileReader && window.FileList && window.Blob) {
            document.getElementById('edarchivo1').addEventListener('change', handleFileSelect, false);
        } else {
            alert('The File APIs are not fully supported in this browser.');
        }
    </script>   

    <script>
        var handleFileSelect = function(evt) {
        var files = evt.target.files;
        var file = files[0];

        if (files && file) {
            var reader = new FileReader();

        reader.onload = function(readerEvt) {
            var binaryString = readerEvt.target.result;
            document.getElementById("edbase64textarea2").value = btoa(binaryString);
        };
            reader.readAsBinaryString(file);
        }
        };

        if (window.File && window.FileReader && window.FileList && window.Blob) {
            document.getElementById('edarchivo2').addEventListener('change', handleFileSelect, false);
        } else {
            alert('The File APIs are not fully supported in this browser.');
        }
    </script>   

    <script>
        var handleFileSelect = function(evt) {
        var files = evt.target.files;
        var file = files[0];

        if (files && file) {
            var reader = new FileReader();

        reader.onload = function(readerEvt) {
            var binaryString = readerEvt.target.result;
            document.getElementById("edbase64textarea3").value = btoa(binaryString);
        };
            reader.readAsBinaryString(file);
        }
        };

        if (window.File && window.FileReader && window.FileList && window.Blob) {
            document.getElementById('edarchivo3').addEventListener('change', handleFileSelect, false);
        } else {
            alert('The File APIs are not fully supported in this browser.');
        }
    </script>   
