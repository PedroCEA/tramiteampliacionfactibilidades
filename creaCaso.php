<?php
	error_reporting(E_ALL ^ E_NOTICE);
	require_once 'conexion.php';

	function multiexplode ($delimiters,$string) {
		$ready = str_replace($delimiters, $delimiters[0], $string);
		$launch = explode($delimiters[0], $ready);
		return  $launch;
	}

	$rfc=$_POST['rfc'];

	$soap_request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" encoding="UTF-8">
	<soapenv:Header/>
		<soapenv:Body>
		<tem:createCasesAsString>
		<!--Optional:-->
			<tem:casesInfo>
			<![CDATA[<BizAgiWSParam>
				<domain>CEA</domain>
				<userName>'.$rfc.'</userName>
					<Cases>
					<Case>
					<Process>CopySolicitudFactibilidadD</Process>
						<Entities>
						<Expediente>
						</Expediente>
						</Entities>
					</Case>
					</Cases>
			</BizAgiWSParam>]]>
			</tem:casesInfo>
		</tem:createCasesAsString>
		</soapenv:Body>
	</soapenv:Envelope>';

    $headers = array(
	"Content-type: text/xml",
	"Accept: text/xml",
	"Cache-Control: no-cache",
	"Pragma: no-cache",
	"SOAPAction: http://tempuri.org/createCasesAsString",
	"Content-length: ".strlen($soap_request),
    );

    //$url = "http://10.1.1.155/SCG/WebServices/WorkflowEngineSOA.asmx";
    $url = "http://10.1.1.67/SCG/WebServices/WorkflowEngineSOA.asmx";

    $ch = curl_init($url);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $soap_request);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_VERBOSE, true);
	curl_setopt($ch, CURLOPT_TIMEOUT,10);
    //echo $soap_request; 
    $output = curl_exec($ch);
    curl_close($ch);
    $datos = multiexplode(array("&lt;","/","&gt;"), $output);
    $caso = $datos['24'];
    $radNumber1=$datos['29'];
    $radNumberA=explode("-",$radNumber1);
    //Array(    [0] => VUD    [1] => MF    [2] => 1544    [3] => 21)    
    $radNumber = $radNumberA[0]."-SD-AF-".$radNumberA[2]."-".$radNumberA[3];
    $strSQL= "UPDATE WFCASE SET radNumber='$radNumber' WHERE idCase=$caso";
    //echo $strSQL;
    $query = sqlsrv_query($conn, $strSQL) or die ("Hemos tenido un problema vuelva a recargar la pagina");
    echo $radNumber;


?>    
